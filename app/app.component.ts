import { Component, HostBinding } from 'angular2/core';
import { Item, ItemService} from './services/item.service';
import { DetailComponent } from './detail.component';

@Component({

	selector: 'item-app',
	templateUrl: 'app/templates/list.html',
	directives: [DetailComponent],
	providers: [ItemService],

})

export class AppComponent {

	private user = 's144858';
	private pass = 'MinNyeKode1234';
	private token: string;
	public errorMessage: string;

	public title = 'DDHF Genstandsadministration';
	public items: Item[];
	selectedItem: Item;

	constructor(private _itemService: ItemService) { }

	getAllItems() {

		if (this.token)
			this._itemService.setApiToken(this.token);
		else
			return false;

		this._itemService.getAllItems().subscribe(res => {

			var rj = res.json();

			if (rj.sanity === 'GOOD')
				this.items = rj.data.default;

		});

	}

	ngOnInit() {

		this.token = localStorage.getItem('token');
		this.getAllItems();

	}

	login() {

		this._itemService.tryLogin(this.user, this.pass).subscribe(res => {

			var rj = res.json();

			console.log(rj.sanity);

			if (rj.sanity !== 'GOOD') {

				this.errorMessage = 'Brugernavn/kodeord ikke korrekt. Prøv igen.';

			} else {

				localStorage.setItem('token', rj.token);
				this.token = rj.token;
				this.getAllItems();

			}

		});

	}

}