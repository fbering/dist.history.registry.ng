import { Component, EventEmitter, HostBinding } from 'angular2/core';
import { Item, ItemService } from './services/item.service';

@Component({

	selector: 'item-detail',
	templateUrl: 'app/templates/detail.html',
	inputs: ['selectedItem'],

})

export class DetailComponent {

	public selectedItem: Item;

	constructor(private _itemService: ItemService) {}

	updateSelectedItem() {

		this._itemService.updateItem(this.selectedItem);

	}

	deleteSelectedItem() {

		this._itemService.deleteItem(this.selectedItem);
		this.selectedItem.deleted_at = "1";

	}

	restoreSelectedItem() {

		this._itemService.deleteItem(this.selectedItem);
		this.selectedItem.deleted_at = null;

	}

}