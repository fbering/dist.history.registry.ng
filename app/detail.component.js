System.register(['angular2/core', './services/item.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, item_service_1;
    var DetailComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (item_service_1_1) {
                item_service_1 = item_service_1_1;
            }],
        execute: function() {
            DetailComponent = (function () {
                function DetailComponent(_itemService) {
                    this._itemService = _itemService;
                }
                DetailComponent.prototype.updateSelectedItem = function () {
                    this._itemService.updateItem(this.selectedItem);
                };
                DetailComponent.prototype.deleteSelectedItem = function () {
                    this._itemService.deleteItem(this.selectedItem);
                    this.selectedItem.deleted_at = "1";
                };
                DetailComponent.prototype.restoreSelectedItem = function () {
                    this._itemService.deleteItem(this.selectedItem);
                    this.selectedItem.deleted_at = null;
                };
                DetailComponent = __decorate([
                    core_1.Component({
                        selector: 'item-detail',
                        templateUrl: 'app/templates/detail.html',
                        inputs: ['selectedItem'],
                    }), 
                    __metadata('design:paramtypes', [item_service_1.ItemService])
                ], DetailComponent);
                return DetailComponent;
            })();
            exports_1("DetailComponent", DetailComponent);
        }
    }
});
//# sourceMappingURL=detail.component.js.map