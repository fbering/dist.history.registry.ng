System.register(['angular2/core', './services/item.service', './detail.component'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, item_service_1, detail_component_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (item_service_1_1) {
                item_service_1 = item_service_1_1;
            },
            function (detail_component_1_1) {
                detail_component_1 = detail_component_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent(_itemService) {
                    this._itemService = _itemService;
                    this.user = 's144858';
                    this.pass = 'MinNyeKode1234';
                    this.title = 'DDHF Genstandsadministration';
                }
                AppComponent.prototype.getAllItems = function () {
                    var _this = this;
                    if (this.token)
                        this._itemService.setApiToken(this.token);
                    else
                        return false;
                    this._itemService.getAllItems().subscribe(function (res) {
                        var rj = res.json();
                        if (rj.sanity === 'GOOD')
                            _this.items = rj.data.default;
                    });
                };
                AppComponent.prototype.ngOnInit = function () {
                    this.token = localStorage.getItem('token');
                    this.getAllItems();
                };
                AppComponent.prototype.login = function () {
                    var _this = this;
                    this._itemService.tryLogin(this.user, this.pass).subscribe(function (res) {
                        var rj = res.json();
                        console.log(rj.sanity);
                        if (rj.sanity !== 'GOOD') {
                            _this.errorMessage = 'Brugernavn/kodeord ikke korrekt. Prøv igen.';
                        }
                        else {
                            localStorage.setItem('token', rj.token);
                            _this.token = rj.token;
                            _this.getAllItems();
                        }
                    });
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'item-app',
                        templateUrl: 'app/templates/list.html',
                        directives: [detail_component_1.DetailComponent],
                        providers: [item_service_1.ItemService],
                    }), 
                    __metadata('design:paramtypes', [item_service_1.ItemService])
                ], AppComponent);
                return AppComponent;
            })();
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.component.js.map