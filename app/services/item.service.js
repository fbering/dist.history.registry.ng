System.register(['angular2/core', 'angular2/http', 'rxjs/Rx'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1;
    var ItemService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {}],
        execute: function() {
            ItemService = (function () {
                function ItemService(http) {
                    this.http = http;
                    this.apiUrl = 'http://fbballin.com/v1';
                }
                ItemService.prototype.setApiToken = function (token) {
                    this.apiToken = token;
                };
                ItemService.prototype.getAllItems = function () {
                    return this.http.get(this.apiUrl + '/items/all?token=' + this.apiToken);
                };
                ItemService.prototype.tryLogin = function (user, pass) {
                    return this.http.get(this.apiUrl + '/login?user=' + user + '&pass=' + pass);
                };
                ItemService.prototype.deleteItem = function (item) {
                    this.body = 'token=' + this.apiToken;
                    var headers = new http_1.Headers();
                    headers.append('Content-Type', 'application/x-www-form-urlencoded');
                    this.http.post(this.apiUrl + '/items/delete/' + item.id.toString(), this.body, {
                        headers: headers
                    }).map(function (res) {
                        console.log("HTTP Status code: " + res.status);
                        return res.json();
                    }).subscribe(function (data) { return console.log(data); }, function (err) { return console.error(err); }, function () { return console.log('POST Complete!'); });
                };
                ItemService.prototype.updateItem = function (item) {
                    //this.body = JSON.stringify(item);
                    this.body = "headline=" + item.headline;
                    this.body += "&description=" + item.description;
                    this.body += "&donator=" + item.donator;
                    this.body += "&producer=" + item.producer;
                    this.body += "&zipcode=" + item.zipcode;
                    this.datingFrom = JSON.stringify(item.dating_from).split(",")[0].substring(11, 21).split("-");
                    this.body += "&dating_from=" + (new Date(Number.parseInt(this.datingFrom[2]), Number.parseInt(this.datingFrom[1]) - 1, Number.parseInt(this.datingFrom[0])).getTime() / 1000 + 7200);
                    this.datingTo = JSON.stringify(item.dating_to).split(",")[0].substring(11, 21).split("-");
                    this.body += "&dating_to=" + (new Date(Number.parseInt(this.datingTo[2]), Number.parseInt(this.datingTo[1]) - 1, Number.parseInt(this.datingTo[0])).getTime() / 1000 + 7200);
                    this.datingReceived = JSON.stringify(item.received_at).split(",")[0].substring(11, 21).split("-");
                    this.body += "&received_at=" + (new Date(Number.parseInt(this.datingReceived[2]), Number.parseInt(this.datingReceived[1]) - 1, Number.parseInt(this.datingReceived[0])).getTime() / 1000 + 7200);
                    this.body += '&token=' + this.apiToken;
                    //console.log("body: " + this.body);
                    var headers = new http_1.Headers();
                    headers.append('Content-Type', 'application/x-www-form-urlencoded');
                    this.http.post(this.apiUrl + '/items/' + item.id.toString(), this.body, {
                        headers: headers
                    }).map(function (res) {
                        console.log("HTTP Status code: " + res.status);
                        return res.json();
                    }).subscribe(function (data) { return console.log(data); }, function (err) { return console.error(err); }, function () { return console.log('POST Complete!'); });
                };
                ItemService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], ItemService);
                return ItemService;
            })();
            exports_1("ItemService", ItemService);
        }
    }
});
//# sourceMappingURL=item.service.js.map